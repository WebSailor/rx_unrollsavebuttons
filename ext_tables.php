<?php

defined('TYPO3_MODE') or die();

if (TYPO3_MODE === 'BE') {
    $settings = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['rx_unrollsavebuttons']);
    if ($settings['allowUserSettings']) {
        $GLOBALS['TYPO3_USER_SETTINGS']['columns']['unrollButtonMode'] = [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'label' => 'LLL:EXT:rx_unrollsavebuttons/Resources/Private/Language/locallang.xlf:userSettings.mode',
            'items' => [
                'default' => 'LLL:EXT:rx_unrollsavebuttons/Resources/Private/Language/locallang.xlf:userSettings.default',
                'disableUnroll' => 'LLL:EXT:rx_unrollsavebuttons/Resources/Private/Language/locallang.xlf:userSettings.disableUnroll',
                'iconOnly' => 'LLL:EXT:rx_unrollsavebuttons/Resources/Private/Language/locallang.xlf:userSettings.iconOnly',
                'iconAndText' => 'LLL:EXT:rx_unrollsavebuttons/Resources/Private/Language/locallang.xlf:userSettings.iconAndText',
            ],
            'csh' => 'EXT_rx_unrollsavebuttons:unrollButtonMode',
        ];
        $GLOBALS['TYPO3_USER_SETTINGS']['showitem'] .= ',
            --div--;LLL:EXT:rx_unrollsavebuttons/Resources/Private/Language/locallang.xlf:userSettings.tabName,unrollButtonMode';
    }
}
